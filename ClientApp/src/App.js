import React, {Component} from 'react';
import {Layout} from "./components/Layout";
import {Route} from 'react-router-dom';
import {Home} from './pages/home/Home';

import './custom.css'

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Route exact path='/' component={Home}/>
            </Layout>
        );
    }
}
