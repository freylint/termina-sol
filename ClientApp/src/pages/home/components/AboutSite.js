import React, {Component} from 'react';

export class AboutSite extends Component {
    render() {
        const react_icon_src = "https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg";
        const bootstrap_icon_src = "https://upload.wikimedia.org/wikipedia/commons/b/b2/Bootstrap_logo.svg";
        const dotnet_icon_src = "https://upload.wikimedia.org/wikipedia/commons/e/ee/.NET_Core_Logo.svg";
        const webpack_icon_src = "https://upload.wikimedia.org/wikipedia/commons/c/c1/Webpack.png";

        const img_height = "100px"
        
        return (
            <section class="col-sm-12 col-md-6 col-xl-4">
                <h2 class="border-bottom">About this site</h2>
                <p>
                    This site is something of a portfolio piece that I use to gain
                    familiarity with the in demand web tech of the day.
                </p>

                <p>It is currently built with:</p>
                <div class="container row">
                    <img className="col-4" src={react_icon_src} width={img_height} alt="React Logo"/>
                    <img className="col-4" src={webpack_icon_src} width={img_height} alt="Webpack Logo"/>
                    <img className="col-4" src={bootstrap_icon_src} width={img_height} alt="Bootstrap Logo"/>
                    <img className="col-4" src={dotnet_icon_src} width={img_height} alt="DotNet Logo"/>
                </div>
            </section>
        );
    }
}
