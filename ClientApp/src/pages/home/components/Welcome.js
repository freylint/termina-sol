import React, {Component} from 'react';

export class Welcome extends Component {
    render() {
        return (
            <section>
                <h1 class="border-bottom">Welcome to Termina-Sol!</h1>
                <p>This is the personal website, blog, and project hosting space of Katherine Vance.</p>
                <p>A home away from the trappings of social media where I talk about what I'm up to.</p>
            </section>
        );
    }
}
