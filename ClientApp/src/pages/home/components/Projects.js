import React, {Component} from 'react';

export class Projects extends Component {
    render() {
        return (
            <section className="col-sm-12 col-md-6">
                <h4 className="border-top-0 border-bottom">Projects made with</h4>
                <li className="list-group-item flex-fill">
                    <p>PLACEHOLDER: RUST LOGO</p>
                    <em>Rust</em>
                    <hr/>
                    <ul>
                        <li>Tokio</li>
                        <li>Serde</li>
                        <li>Bevy Engine</li>
                        <li>Wgpu</li>
                        <li>Winit</li>
                    </ul>
                </li>
                <li className="list-group-item flex-fill">
                    <p>PLACEHOLDER GODOT LOGO</p>
                    <em>Godot</em>
                    <hr/>
                </li>
                <li className="list-group-item flex-fill">
                    <p>PLACEHODLER DOTNET LOGO</p>
                    <em>Dotnet</em>
                    <hr/>
                    <ul>
                        <li>Unity</li>
                        <li>Monogame</li>
                    </ul>
                </li>
            </section>
        );
    }
}
