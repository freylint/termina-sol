import React, {Component} from 'react';

import {Welcome} from "./components/Welcome";
import {AboutSite} from "./components/AboutSite";
import {AboutMe} from "./components/AboutMe";
import {HireMe} from "./components/HireMe";
import {Blogs} from "./components/Blogs";
import {Projects} from "./components/Projects";

export class Home extends Component {
    static displayName = Home.name;

    render() {

        return (
            <div class="container">
                <Welcome class="row col-12"/>

                <div class="row">
                    <AboutMe/>
                    <AboutSite/>
                    <HireMe/>
                </div>

                <div class="row">
                    <Blogs/>
                    <Projects/>
                </div>
            </div>
        );
    }
}
